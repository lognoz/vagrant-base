# -*- mode: ruby -*-
# vim: set ft=ruby :
# ============================================================================
# File:        Vagrantfile
# Author:      Marc-Antoine Loignon <info@lognoz.com>
# Licence:     GPL licence
# Website:     https://www.gitlab.com/lognoz/vagrant-base
# Version:     1.0
#
#  Permission is hereby granted to use and distribute this code, with or
#  without modifications, provided that this copyright notice is copied with
#  it. Like anything else that's free, this vagrant file is provided *as is*
#  and comes with no warranty of any kind, either expressed or implied. In no
#  event will the copyright holder be liable for any damamges resulting from
#  the use of this software.
# ============================================================================

required_plugins = %w( vagrant-vbguest vagrant-hostmanager )
required_plugins.each do |plugin|
  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
end

Vagrant.configure("2") do |config|
	config.vm.box = "ubuntu/xenial64"
	config.vm.network "private_network", ip: "192.168.33.10"
	config.vm.synced_folder ".", "/var/www/", :mount_options => [ "dmode=777", "fmode=666" ]
	config.vm.synced_folder "./provision/", "/home/vagrant/"

	config.vm.provider "virtualbox" do |vb|
		vb.memory = "1024"
		vb.name = "tmp"
	end

	config.hostmanager.enabled = true
	config.hostmanager.manage_host = true
	config.hostmanager.manage_guest = true
	config.hostmanager.ignore_private_ip = false
	config.hostmanager.include_offline = true

	config.vm.define 'tmp-box' do |node|
		node.vm.hostname = 'tmp-box-hostname'
		node.vm.network :private_network, ip: '192.168.42.42'
		node.hostmanager.aliases = [ "tmp.local", "www.tmp.local" ]
	end

	config.vm.provision :hostmanager
	config.vm.provision :shell, path: "provision/script/bootstrap.sh"
	config.vm.provision :shell, path: "provision/script/database.sh"
end
