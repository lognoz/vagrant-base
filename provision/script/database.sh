#!/usr/bin/env bash

# ============================================================================
# File:        database.sh
# Author:      Marc-Antoine Loignon <info@lognoz.com>
# Licence:     GPL licence
# Website:     https://www.gitlab.com/lognoz/vagrant-base
# Version:     1.0
#
#  Permission is hereby granted to use and distribute this code, with or
#  without modifications, provided that this copyright notice is copied with
#  it. Like anything else that's free, this vagrant file is provided *as is*
#  and comes with no warranty of any kind, either expressed or implied. In no
#  event will the copyright holder be liable for any damamges resulting from
#  the use of this software.
# ============================================================================

database-source() {
	if [ ! -f $APP_SOURCE/*.sql ]; then
		exit 1
	else
		for path in $APP_SOURCE/*.sql; do
			name=$(basename $path .sql)
			last_modification=$(date -r $path +%s)
			echo "$name:$last_modification"
		done
	fi
}

mysql-dump() {
	local database=($(echo $1 | tr ":" "\n"))

	mysql -uroot --password="root" -e "DROP database IF EXISTS $database;" &> /dev/null
	mysql -uroot --password="root" -e "CREATE database $database;" &> /dev/null
	mysql -uroot --password="root" $database < $APP_SOURCE/$database.sql &> /dev/null
}

write-last-modification-file() {
	printf "%s\n" "$@" > $APP_DATABASE_CONFIG
}

main() {
	declare APP_SOURCE="$1" APP_DATABASE_CONFIG="$2"
	local source="$(database-source)" setting=()

	if [ -s $APP_DATABASE_CONFIG ]; then
		setting=$(<$APP_DATABASE_CONFIG)
	fi

	for i in $source; do
		for j in $setting; do
			if [ $j = $i ]; then
				continue 2
			fi
		done
		echo $i
		mysql-dump "$i"
	done

	write-last-modification-file $source
}

main "/home/vagrant/database/" "/home/vagrant/database/last_modification"
