#!/usr/bin/env bash
# ============================================================================
# File:        bootstrap.sh
# Author:      Marc-Antoine Loignon <info@lognoz.com>
# Licence:     GPL licence
# Website:     https://www.gitlab.com/lognoz/vagrant-base
# Version:     1.0
#
#  Permission is hereby granted to use and distribute this code, with or
#  without modifications, provided that this copyright notice is copied with
#  it. Like anything else that's free, this vagrant file is provided *as is*
#  and comes with no warranty of any kind, either expressed or implied. In no
#  event will the copyright holder be liable for any damamges resulting from
#  the use of this software.
# ============================================================================

install-phpmyadmin() {
	debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password ''"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password ''"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password ''"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"

	apt-get install -y phpmyadmin &> /dev/null
}

install-php() {
	apt-get install -y php7.1 php7.1-fpm
	apt-get install -y php7.1-mysql
	apt-get install -y mcrypt php7.1-mcrypt
	apt-get install -y php7.1-cli php7.1-curl php7.1-mbstring php7.1-xml php7.1-mysql
	apt-get install -y php7.1-json php7.1-cgi php7.1-gd php-imagick php7.1-bz2 php7.1-zip
}

update-package() {
	apt-get update &> /dev/null
}

add-repository() {
	add-apt-repository ppa:ondrej/php -y &> /dev/null
	add-apt-repository ppa:nijel/phpmyadmin &> /dev/null
}

restart-service() {
	service apache2 restart
	systemctl start php7.0-fpm
}

install-apache() {
	apt-get install -y apache2 libapache2-mod-fastcgi apache2-mpm-worker
	echo "ServerName localhost" > /etc/apache2/httpd.conf

	a2enmod actions fastcgi rewrite
	service apache2 restart
}

echo "Add repositories..."
add-repository

echo "Update package lists..."
update-package

echo "Install Apache2..."
install-apache

echo "Install Php..."
install-php

echo "Install phpmyadmin..."
install-phpmyadmin

echo "Restart services..."
restart-service
